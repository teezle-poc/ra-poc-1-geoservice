FROM node:9-slim
ENV PORT 8642
EXPOSE 8642
WORKDIR /usr/src/app
COPY . .
CMD ["npm", "start"]